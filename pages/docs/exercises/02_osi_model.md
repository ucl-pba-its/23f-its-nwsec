---
hide:
  - footer
---

# Opgave 2 - OSI model

## Information

OSI model har 7 lag.

## Instruktioner

1. Find almindelige (consumer) eksempler på hardware enheder, og placer dem på OSI modellen.
2. Find nye/sjældne/esoteriske enheder (fysiske eller virtuelle) (med referencer) - f.eks. next-gen firewalls, proxies, application gateways. Hvad-som-helst med et netstik/wifi er ok at have med.
3. Forbered jer til diskussion/præsentation af hvad i har fundet så vi kan dele viden på klassen.

## Links

Links om OSI modellen se: 
- [moozer](https://moozer.gitlab.io/course-networking-basisc/02_OSI/osi_model/)
- [comparitech](https://www.comparitech.com/net-admin/osi-model-explained/)
- [techterms](https://www.youtube.com/watch?v=vv4y_uOneC0)