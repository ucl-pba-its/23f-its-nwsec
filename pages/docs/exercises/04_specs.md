---
hide:
  - footer
---

# Opgave 4 - specs

## Information

Vi vil se på specs for forskelligt hardware, og sammenligne.

## Instruktioner

1. Find listen fra ww06, hvor lavede vi en liste over hardware og deres plads i OSI modellen.
2. Udvælg 2-3 forskellige enheder fra listen
2. Find specifikationerne for enheden
4. Hvad bliver listet som begrænsninger? Synes du det er høje tal? lave tal?

## Links

Som eksempel, [datablad for Junipe SRXes](https://www.juniper.net/us/en/products/security/srx-series/srx300-line-services-gateways-branch-datasheet.html)