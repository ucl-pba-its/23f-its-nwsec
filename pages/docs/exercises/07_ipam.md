---
hide:
  - footer
---

# Opgave 7 - ipam

## Information

IPAM - (IP Address Management)

Læs [Companion](https://moozer.gitlab.io/course-networking-security/03_network_layer/) for detaljeret information.

Link til [Netbox VM (.ova fil)](https://drive.google.com/file/d/1ZfPr_P2llgnJZsvLj50ZHaIAsgH8b1AA/view?usp=sharing)

## Instruktioner

1. papir+blyant øvelse.  
    Lav en IP subnet oversigt over dine devices og de tilhørende interne og eksterne netværk.  
    Anvend meget gerne det netværk i påtænker til semester projektet!
2. Research hvad netbox er, lav en liste over hvad det kan og hvad det ikke kan.

3. Set netbox VM op og dokumentér dit netværk via netbox interfacet. Brug [getting started](https://docs.netbox.dev/en/stable/getting-started/planning/) fra netbox dokumentationen (som er lavet med mkdocs!!)  
```
Netbox credentials:  
consolen viser ip adressen
netbox:netbox som password til web interface.
andre logins i console
```
    
    1. Hvis i allerede har netværket til semester projektet skitseret eller tegnet som netværksdiagram så anvend det
    2. Alternativt kig på netværket i den virksomhed du er ansat i 
    3. Ellers søg på nettet efter *large network diagram* eller *large network topology* 


4. Hvis i bruger en virksomhed i opgaven så se på det IPAM dokumentation som virksomheden allerede har og forhold dig til det.  
Alternativt research hvilke andre måder i kan lave IPAM dokumentation på. Noter det som en liste i jeres dokumentation.

## Links

- [netbox dokumentation](https://netbox.readthedocs.io/en/stable/)

