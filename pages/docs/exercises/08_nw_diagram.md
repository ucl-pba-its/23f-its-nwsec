---
hide:
  - footer
---

# Opgave 8 - Netværksdiagrammer

## Information

Netværksdiagrammer kan tegnes som fysiske og logiske diagrammer.

Overblik er en essentiel del i forhold til at implementere sikkerhed, hvis ikke vi har overblik er det svært at vurdere hvor sikkerhed skal implementeres i et netværk.  

Formålet med denne opgave er at få et værktøj til at lave overskuelige netværksdiagrammer samt at få øvet hvordan de tegnes.
Formålet er også at få repeteret fundamentale begreber som IP adresser, VLAN's og forskellen på fysiske og logiske diagrammer.

![netværks diagram skitse](../images/fysisk_logisk_nw_diagram_skitse.jpg)

Opgaven er individuel

## Instruktioner

1. Ovenstående diagram skal laves pænt - ie. find et passende diagram værktøj, og lav Jeres egen version af det.
2. Tilføj noget associeret tekst der beskriver hvad man ser og de vigtige elementer
3. Find 3 spørgsmål til diagrammet og tag dem med til næste undervisningsgang - Det er ok at have fundet svaret på forhånd, så skal du bare notere spørgsmål+svar (inkl. referencer).

## Links

- none
