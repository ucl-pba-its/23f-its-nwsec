---
hide:
  - footer
---

# Opgave 10 - Trafikanalyse

## Information

Netværkstrafik er en vigtig del af sikkerheds arbejdet, det viser ret detaljeret hvad der foregår på netværket og kan bruges til at identificere uønsket trafik.  

Wireshark kan også bruges til at undersøge hvad der historisk er foregået på netværket, dette anvedes blandt andet til at forstå hvordan trusselsaktører har udfært, eller forsøgt at udføre et angreb.  

Netværkstrafik kan gemmes som PCAP som wireshark kan skrive/læse.

Husk at du kan bruge wireshark til at følge f.eks en TCP stream.  
Når du kigger på netværkstrafik er det også nyttigt at bruge filtre i wireshark.  

## Instruktioner

1. Start wireshark på lokal net/firma net og saml noget data op
2. Gem PCAPS
3. Se på det: er der ukendte protokoller? ukendte ip adresser? andet interessant?
4. Dyk ned i 2-3 streams, og forstå hvad der foregår.
5. Tag noter og vis det næste gang

## Links

- [PCAP](https://en.wikipedia.org/wiki/Pcap)
- [wireshark display filters](https://www.wireshark.org/docs/dfref/)
- [Detecting Network Attacks with Wireshark](https://www.infosecmatter.com/detecting-network-attacks-with-wireshark/)
- [Capture Passwords using Wireshark](https://www.infosecmatter.com/capture-passwords-using-wireshark/)
