---
hide:
  - footer
---

# Opgave 13 - THM digdug

## Information

Nu ved du en del om DNS og derfor er det tid til at have lidt sjov med `dig`  
TryHackMe har en lille ctf style opgave der omhandler en DNS server - se om du kan få fat i flaget :-)

Formålet med opgaven er at få afprøvet din viden om DNS i praksis (og have det sjovt imens)

## Instruktioner

Løs opgaven i digdug rummet [https://tryhackme.com/room/digdug](https://tryhackme.com/room/digdug)

## Links

- dig man pages [https://linux.die.net/man/1/dig](https://linux.die.net/man/1/dig)