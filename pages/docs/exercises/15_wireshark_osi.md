---
hide:
  - footer
---

# Opgave 15 - wireshark OSI

## Information

Vi vil lave DNS opslag, genfinde dem i wireshark og se på hvad wireshark fortæller om OSI lagene.

Formålet med opgaven er at konsolidere viden om OSI modellen og få lidt mere praktisk erfaring med wireshark.

## Instruktioner

1. Start din Kali maskine og check at den har internet forbindelse
2. Start wireshark og lyt på det udvendige interface
3. Start en kommando prompt og brug `dig` til at lave et opslag på f.eks. `dr.dk`
4. Gennemgå trafiken i wireshark og fortæl hvad der ses
5. Tegn et netværks diagram med de vigtigste enheder
6. Ud fra wireshark, find ip adresser og MAC adresser på disse enheder

## Links

- dig man pages [https://linux.die.net/man/1/dig](https://linux.die.net/man/1/dig)