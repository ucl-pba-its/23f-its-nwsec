---
hide:
  - footer
---

# Opgave 16 - wireshark NAT

## Information

Denne opgave minder om [opgave 15](15_wireshark_osi.md)
Formålet med opgaven er at kunne arbejde med wireshark.

## Instruktioner

1. Start din Kali maskine og en router, check at der er internet forbindelse
2. Tegn et netværksdiagram med de relevante enheder
3. Find to egnede steder i netværket hvor man kan lytte til host trafik og til ekstern internet trafikken
2. Start to instances af `wireshark` eller `tcpdump` der lytter på de valgte punkter
4. Connect til en hjemmeside fra kalien. Brug `curl` eller `wget` for at reducere mængden af trafik
5. Genfind trafikken i begge wireshark streams
6. Vis forskellene på lag 2 og 3

## Links

- ingen pt.