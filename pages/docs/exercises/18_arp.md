---
hide:
  - footer
---

# Opgave 18 - ARP basics

## Information

Ønsker man at lave man-in-the-middle angreb på lag 2, så er ARP protokollen relevant.

I denne op gave ser vi på hvordan den virker.

## Instruktioner

1. Start Kali i vmnet8
2. Start wireshark i kali
3. Find ARP requests

    Forklar hvad der ses

4. Ping en ikke-eksisterende ip adresse og find arp requests

    Forklar hvad der ses

5. Kør `arp -e`

    Forklar hvad der ses


