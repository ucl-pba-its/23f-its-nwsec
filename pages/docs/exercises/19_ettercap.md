---
hide:
  - footer
---

# Opgave 19 - Ettercap

## Information

Ettercap er et tool til at lave man-in-the-middle angreb med.


## Instruktioner

1. Start to Kali VM i vmnet8

    Den første Kali kalder vi "kali" og den anden "victim"

2. Find MAC og ip adresserne på både kali og victim

    Notér dem ned

3. Start wireshark i kali

4. Start `ping 8.8.8.8` på victim

    Kan det ses i wireshark på kali?

    Hvorfor Ja? hvorfor nej?

4. Start ettercap

    Add Victm and target and router as target 2.

    Start ARP poisoning

4. Start `ping 8.8.8.8` på victim

    Kan det ses i wireshark på kali?

    Hvorfor Ja? hvorfor nej? Er det noget at bemærke?

5. Forklar hvad ettercap gør





