---
hide:
  - footer
---

# Opgave 20 - Ettercap DNS

## Information

Ettercap er et tool til at lave man-in-the-middle angreb med. Den kan også lave DNS spoofing.


## Instruktioner

1. Start to Kali i vmnete8

    Den første Kali kalder vi "kali" og den anden "victim"

    Bruges OpenBSD routeren, så får man problemer

2. Tilføj følgende linier i `/etc/ettercap/etter.dns`

    ```
    totallynotgoogle-B.dns PTR 8.8.4.4
    totallynotgoogle-A.dns PTR 8.8.8.8
    dnsspoof.test A 8.8.8.8 3600
    ```

4. Start ettercap

    Stop sniffing

    Scan for ip adresser

    Add Victim as target 1 and router as target 2.

    Enable "dns_spoof" plugin

    Start ARP poisoning

    Start sniff

4. Test at spoofing virker: `ping dnsspoof.test`

4. Start `ping dns.google` på victim

    Kan det ses i wireshark på kali? Er der noget at bemærke i output eller i terminalen?

    Forklar

5. Forklar hvad ettercap gør


NB. Ettercap skal genstartes når man opdaterer `etter.dns`.



