---
hide:
  - footer
---

# Opgave 22 - Routing

## Information

"Routing" er den mekanisme på lag 3 som håndterer at sende pakker på tværs af subnets.

Denne opgave er om at læse routing tables.


## Instruktioner

1. Start Kali
2. Check at der er internet adgang
3. Kør `ip a` og forklar hvad der ses
4. Kør `ip route` og forkalr hvad der ses
5. Tegn er netværksdiagram med de relevante enheder og ip adresser
6. Gør der samme på hosten.
  * på windows ses ip adresser vha. `ipconfig` og routing informationer med `route print`