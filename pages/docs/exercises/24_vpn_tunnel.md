---
hide:
  - footer
---

# Opgave 24 - VPN

## Information

VPN er krypterede tunneller. Vi vil bruge wireguard til at illustrere hvordan det virker.


## Instruktioner

1. Start 2x kali på det samme subnet
2. ping 8.8.8.8 på begge og genfind trafik i wireshark
3. Tegn et netværksdiagram med relevant enheder
4. set up wg client på den ene and server på den anden
  * Der er en guide [her](https://linuxhint.com/install-configure-wireguard-kali-linux/)
  * Brug `172.16.100.1/24` til serveren og `172.16.100.2/32` til klienten
  * Tilføj en route på klienten `sudo ip route add 8.8.8.0/8 via 172.16.100.1`
5. Ping 172.16.100.1 fra klienten og genfind trafik
5. Ping 8.8.8.8 fra client og genfind trafik.
  * virker tunnelen?
6. updater netværksdiagram med nye ip adresser og interfaces
7. Se på routing tables og foklar hvad der ses

