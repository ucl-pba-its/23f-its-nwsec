---
hide:
  - footer
---

# Opgave 26 - IDS/IPS viden

## Information

Intrusion detection er detektering af uønsket netværkstrafik og intrusion prevention er forhindring af uønsket netværkstrafik.  
Der findes overordnet 2 typer af detektering, signatur baseret eller anomali baseret.  

I denne opgave skal i undersøge hvad intrusion detection og prevention er og hvad forskellen er på signatur baseret og anomali baseret.

Opgaven skal løses i jeres teams og dokumenteres på gitlab.  

## Instruktioner

Brug nedenstående links til at besvare følgende spørgsmål:  

1. Forklar med egne ord hvad et intrusion detection system gør, underbyg med troværdige kilder (links)
2. Forklar med egne ord hvad et intrusion prevention system gør, underbyg med troværdige kilder (links)
3. Forklar forskellen på et Netværks IDS/IPS (NIDS) og et host IDS/IPS (HIDS)
4. Forklar med egne ord hvad signatur baseret detektering er, underbyg med troværdige kilder (links)
5. Forklar med egne ord hvad anomali baseret detektering er, underbyg med troværdige kilder (links)
6. Forklar mulige problemer med hhv. signatur og anomali baseret detektering

I må gerne medtage anden viden i opnår igennem opgaven, for eksempel ved at søge på nettet efter ting i ikke umiddelbart forstår eller bare gerne vil vide mere om.  
Der er et par links herunder der kan hjælpe med at finde svar på ovenstående spørgsmål.

## Links

Viden:  

- [Ross Anderson - Security engineering afsnit 21.4.3, 21.4.4](https://www.cl.cam.ac.uk/~rja14/Papers/SEv2-c21.pdf) (lidt gammel 2. udgave men ok)
- [Wikipedia](https://en.wikipedia.org/wiki/Intrusion_detection_system)
- [Palo Alto - What is an Intrusion Prevention System?](https://www.paloaltonetworks.com/cyberpedia/what-is-an-intrusion-prevention-system-ips)
- [Video: IDS vs IPS: Which to Use and When](https://youtu.be/wQSd_piqxQo)

Eksempler på systemer:  

- [Suricata](https://suricata.io/)  
- [Crowdsec](https://doc.crowdsec.net/)  
