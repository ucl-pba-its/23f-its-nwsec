---
hide:
  - footer
---

# Opgave 30 - Certifikater

## Information

Certifikater er et af fundamenterne i det moderne web.

## Instruktioner

1. Gå til [https://dr.dk](https://dr.dk)
2. Klik på hængelåsen ved siden af url'en, og vis certifikat information
3. Hvem har udstedt certifikatet, hvem er det udstedt til og hvad er levetiden?

  [Certificate chains](https://knowledge.digicert.com/solution/SO16297.html) er relevant her

4. Gå til [https://ucl.dk](https://ucl.dk)
5. Klik på hængelåsen ved siden af url'en, og vis certifikat information
6. Hvem har udstedt certifikatet, hvem er det udstedt til og hvad er levetiden?
7. Start en kali
8. Åben en terminal og kør `openssl s_client ucl.dk:443`
    
    Brug `ctrl+c` til at stoppe kommandoen

9. Sammenlign informationen fra browseren

## Links

* [RFC5280](https://www.rfc-editor.org/rfc/rfc5280) om PKI
* [x509 explainer video](https://www.youtube.com/watch?v=kAaIYRJoJkc)
* `openssl` programmet kan meget se e.g. [her](https://www.ssldragon.com/blog/what-is-openssl/)