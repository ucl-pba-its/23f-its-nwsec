---
hide:
  - footer
---

# Opgave 35 - Netværkssegmentering

## Information

Segmentering af netværket

En MITRE att&ck mitigation er [network segmentation](https://attack.mitre.org/mitigations/M1030/)

## Instruktioner

1. Hvad er netværkssegmentering? Hvad bruges det til?

2. Konstruér et simple netværk med DMZ zone og brugere. 

3. Gennemgå netværket med en CIA indfaldsvinkel, og beskriv problemerne.

4. Beskriv hvilke problemer i netværket som netværkssegmentering reducerer.


## Links

* [MITRE att&ck framework](https://attack.mitre.org/)