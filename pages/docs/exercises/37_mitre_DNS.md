---
hide:
  - footer
---


# Opgave 37 - DNS

## Information

Vi har tidligere snakket om problemer med ARP og spoofing. Se f.eks. opgaverne med [ettercap](19_ettercap.md)

Det er også med i MITRE att&ck framework.


## Instruktioner

1. DNS er meget brugt i angreb
    * [DNS og C2](https://attack.mitre.org/techniques/T1071/004/)
    * [recon](https://attack.mitre.org/techniques/T1590/002/)
    * [attacker DNS server](https://attack.mitre.org/techniques/T1583/002/)
    * [compromised DNS](https://attack.mitre.org/techniques/T1584/002/)
    * [Passive DNS](https://attack.mitre.org/techniques/T1596/001/)
    * Der er flere. Søg selv efter DNS.

2. Er det noget der bliver brugt af angriberne?

3. Hvad er der er mitigeringer?

4. Hvad er der af detection?

5. Hvordan kan suricata og ssl inspection bruges i denne kontekst?

## Links

Se ovenfor.
