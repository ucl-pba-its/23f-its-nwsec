---
hide:
  - footer
  - toc
---

# UCL PBa IT Sikkerhed

På dette website finder du ugeplaner, opgaver, dokumenter og links til brug i faget  
**Netværks- og kommunikationssikkerhed forår 2023** 

Semestrets lektionsplan finder du under [dokumenter](https://ucl-pba-its.gitlab.io/23f-its-nwsec/other_docs/lektionsplan/).  
[Ugeplaner](https://ucl-pba-its.gitlab.io/23f-its-nwsec/weekly/ww_06/) giver dig en oversigt over fagets indhold, uge for uge.  
Under [opgaver](https://ucl-pba-its.gitlab.io/23f-its-nwsec/exercises/01_learning_goals/) kan du se fagets opgaver, det vil dog forekomme at opgaver også er beskrevet på eksterne sites samt itslearning, der henvises til relevante opgaver og hvor du kan finde dem i ugeplanerne.

## Fagets indhold

Faget går ud på at forstå og håndtere netværkssikkerhedstrusler samt implementere og
konfigurere udstyr til samme.  
Faget omhandler forskelligt sikkerhedsudstyr (IDS) til monitorering.  
Derudover vurdering af sikkerheden i et netværk, udarbejdelse af plan til at lukke eventuelle sårbarheder i netværket samt
gennemgang af forskellige VPN teknologier.

### Studieordning

Faget er på **10 ECTS point** og afsluttes med en **individuel mundtlig eksamen** med **ekstern censor**.  

Fagets omfang er i detaljer beskrevet i uddannelsens studieordning.  

Studieordningen er i 2 dele, en national del og en institutionel del.  

Begge dele kan findes i studiedokumenter, på ucl.dk [https://www.ucl.dk/studiedokumenter/it-sikkerhed](https://www.ucl.dk/studiedokumenter/it-sikkerhed)

## Læringsmål fra studieordningen

### Viden

**_Den studerende har viden om og forståelse for_**  

- Netværkstrusler
- Trådløs sikkerhed
- Sikkerhed i TCP/IP
- Adressering i de forskellige lag
- Dybdegående kendskab til flere af de mest anvendte internet protokoller (ssl)
- Hvilke enheder, der anvender hvilke protokoller
- Forskellige sniffing strategier og teknikker
- Netværk management (overvågning/logning, snmp)
- Forskellige VPN setups
- Gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI)

### Færdigheder

**_Den studerende kan_**

- Overvåge netværk samt netværkskomponenter, (f.eks. IDS eller IPS, honeypot)
- Teste netværk for angreb rettet mod de mest anvendte protokoller
- Identificere sårbarheder som et netværk kan have.

### Kompetencer

**_Den studerende kan håndtere udviklingsorienterede situationer herunder_**

- Designe, konstruere og implementere samt teste et sikkert netværk
- Monitorere og administrere et netværks komponenter
- Udfærdige en rapport om de sårbarheder et netværk eventuelt skulle have (red team report)
- Opsætte og konfigurere et IDS eller IPS

<!-- ![Image light theme](images/UCL_horisontal_logo_UK_rgb.png#only-light){ width="300", align=right }
![Image dark theme](images/UCL_horisontal_logo_UK_neg_rgb.png#only-dark){ width="300", align=right } -->
