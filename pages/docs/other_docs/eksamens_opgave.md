---
title: '23F PBa it sikkerhed'
subtitle: 'Eksamen netværks- og kommunikationssikkerhed'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>', Morten Bo Nielsen]
main_author: 'Nikolaj Simonsen'
email: 'nisi@ucl.dk'
left-header: \today
right-header: Eksamen netværks- og kommunikationssikkerhed
semester: 23F
---

# Dokumentets indhold

Dette dokument indeholder praktiske informationer samt emner til eksamen i faget *Netværks- og kommunikationssikkerhed* afholdt i forårssemestret 2023

# Eksamens beskrivelse

Eksamen er beskrevet i den institutionelle del af studieordningen _afsnit 5.2.4_

Tidsplan for eksamen kan findes på wiseflow.  
Eksamen er med ekstern bedømmelse og bedømmes efter 7-trinsskalaen.  

Tidligere dokumenter:  

- [Semesterplanen](https://esdhweb.ucl.dk/D23-2156677.pdf)
- [Fagets hjemmeside](https://ucl-pba-its.gitlab.io/23f-its-nwsec/)
- [Ugeplaner](https://ucl-pba-its.gitlab.io/23f-its-nwsec/other_docs/lektionsplan/)
- [Studieordningen mm.](https://www.ucl.dk/studiedokumenter/it-sikkerhed)

*Fra studieordningen*

Prøven er en individuel, mundtlig prøve med udgangspunkt i et spørgsmål, som den studerende trækker til eksamen.  

Alle spørgsmål, der kan trækkes til eksamen, er udleveret til de studerende senest 14 dage før eksamen, så de studerende har mulighed for at forberede sig. Hvad angår spørgsmålene, vil der blive lagt vægt på, at den studerende kan inddrage eksempler fra projektarbejdet og praktiske øvelser fra det forgangne semester. Der er ingen forberedelse på selve dagen.

Den individuelle, mundtlige eksamen varer 25 minutter inkl. votering

Prøven bedømmes efter 7-trinsskalaen med ekstern bedømmelse.

# Eksamens datoer

- Forsøg 1 - 2023-06-08/09
- Forsøg 2 - 2023-06-22/23
- Forsøg 3 - 2023-08-14

# Underviseres forventning til eksamen

Generelt set, så ønsker vi at se og høre, at den studerende har en god forståelse for hvordan forskellige typer data flyder rundt i et netværk og kan relatere det til sikkerhed. Det foreslås at der laves et simpelt netværkseksempel eller scenarie, som dækker hvad I ønsker at sige.

Der trækkes et emne, og det forventes, at den studerende præsenterer i ca. 10 minutter, hvorefter der vil være spørgsmål. Eksaminator og censor vil  søge at afdække den studerendes niveau i forhold til læringsmålene med udgangspunkt i det trukne emne.

Det anbefales den studerende at have forberedt slides til hvert emne/spørgsmål.

# Emneliste

Nedenstående er listen over emner som kan trækkes på eksamensdagen.  
For alle emner er "i et sikkerhedsperspektiv" underforstået. 

1. **Krypteret trafik:** Formål, muligheder/begrænsninger, performance, aflytte plaintext/ciphertext/metadata
2. **Segmentering:** Routable og non-routable subnets, tunneller, firewalls, L2 security, performance/DOS, lateral movement
3. **Monitorering:** (funktionel): Beskriv de 3 typer (rød/grøn, grafer og logs) og hvad de bruges til både reaktivt og proaktivt
4. **DNS:** Resolve hostnavne, kryptering, certifikater, DNS og privacy
5. **Firewalls:** Funktion i netværket, statefull/stateless, perimeter sikkerhed vs. layered security
6. **SSL inspection:** Implementation, certifikater, placering i netværket, problemer/muligheder for netværks ejeren og brugere
7. **IPS og IDS:** Funktion, teknologi, use cases, placering i netværk hostbaseret/netværksbaseret, signatur/anomali detektering, eksempler på regler
