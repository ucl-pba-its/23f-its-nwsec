---
title: '23F PBa it sikkerhed'
subtitle: 'Fagplan for netværks- og kommunikationssikkerhed'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>', Morten Bo Nielsen]
main_author: 'Nikolaj Simonsen'
email: 'nisi@ucl.dk'
left-header: \today
right-header: Fagplan for netværks- og kommunikationssikkerhed
skip-toc: false
semester: 23F
hide:
  - footer
---

# Lektionsplan

| Uge | Underviser | Indhold                                                                                          |
| --- | ---------- | ------------------------------------------------------------------------------------------------ |
| 06  | MON/NISI   | Introduktion til faget. Tooling up: VMs og linux.                                                |
| 07  | MON        | Forstå netværk - OSI modellen og network hardware: routere, switch, AP, 802.3, 802.11, firewalls |
| 08  | MON        | Netværkstopologi: subnets, IP adresser, VLANs                                                    |
| 09  | NISI       | Bygge netværk, IPAM, netflow                                                                     |
| 10  | NISI       | Netværks protokoller: Wireshark, sniffe trafik, HTTP, HTTPS, m. fl.                              |
| 11  | MON        | Monitorering: SNMP, syslog, Prometheus, Grafana                                                  |
| 12  | NISI       | Network services og applikationer: DNS, Load balancers, Proxy                                    |
| 13  | MON        | Repetition: Wireshark (basics, OSI, NAT) og logning                                              |
| 15  | MON        | Sikkerhed på netværk - lag 1 og 2 (ARP, DNS, Wireless)                                                               |
| 16  | MON        | Routing, tunneller, CIA på lag 3 og 4                                                                 |
| 17  | NISI       | Intrusion Detection og Prevention (IDS/IPS)                                                      |
| 18  | MON        | Hardware sikkerhed lag 5+: application layer security. NG firewalls. Certificates                |
| 19  | MON        | Defending networks: segmentation, DMZ, non-routable networks, lateral movement                           |
| 20  | NISI       | Network monitoring: sniffing, DNS/NS, scanning, shodan, nmap                                     |
| 21  | MON/NISI   | Eksamens recap, og emne gennemgang                                                               |

# Studieaktivitets modellen

![study activity model](Study_Activity_Model.png)

## Andet

Intet på nuværende tidspunkt
