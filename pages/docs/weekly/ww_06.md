---
Week: 06
Content: XXX
Material: xxx
Initials: XXXX
# hide:
#  - footer
---

# Uge 06 - Introduktion

Dette er første undervisning i netværk- og kommunikationssikkerhed.   
Vi skal lære hinanden og faget at kende.  

## Emner

### Ugens emner er

- Introduktion til faget. 

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- Opgave 1, 2 og 3 gennemført, se [opgaver](https://ucl-pba-its.gitlab.io/23f-its-nwsec/exercises/01_learning_goals/)

### Læringsmål der arbejdes med i faget denne uge

#### Overordnede læringsmål fra studie ordningen

- **_Den studerende har viden om og forståelse for_**  

    - Hvilke enheder, der anvender hvilke protokoller
    - Gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI)

#### Læringsmål den studerende kan bruge til selvvurdering

- Den studerende har viden om hvad faget går ud på
- Den studerende kender til OSI modellen og struktureret fejlfinding
- Undervisere kender de studerendes niveau i relation til faget

## Afleveringer

- Opgaver dokumenteret på studerendes gitlab

## Skema

### Mandag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 8:15  | Introduktion til dagen |
| 9:00  | Øvelser K1/K4          |
| 11:30 | Frokost                |
| 12:15 | Øvelser K2/K3          |
| 15:30 | Fyraften               |

## Kommentarer

- 4 lektioner med underviser, resten af dagen opgaver og selvstudie

## Links

- Ingen pt.
