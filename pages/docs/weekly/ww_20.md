---
Week: 20
Content: XXX
Material: xxx
Initials: XXXX
# hide:
#  - footer
---

# Uge 20 - Aktiv monitorering

## Emner

Aktiv monitorering kaldes også scanning. Dette er ofte støjende og nemt at opdage. Det er også et godt værktøj for en administrator, som ønsker at vide hvad der er på deres netværk.

Dagens er workshop baseret og forventningen er at vi når igennem de 3 første opaver i undervisningen.

Opgave 41 er den eneste opgave i har for til næste gang, der bliver altså ikke gennemgang af dagens øvrige opgaver i næste uge.

## Ugens emner er

- Angrebsflade (Attack Surface)
- Årsager til huller i angrebsfladen
- Best practice ifht. angrebsflade
- NMAP og andre lokale værktøjer
- Shodan og andre eksterne værktøjer

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- [Opgave 38: Angrebsflade (attack Surface)](../exercises/38_attack_surface.md)
- [Opgave 39: Huller i suppen](../exercises/39_attack_surface_reasons.md)
- [Opgave 40: Aktive skanningsværktøjer](../exercises/40_active_scanning_tools.md)
- [Opgave 41: Repetitions forberedelse](../exercises/41_recap_prep.md) 

### Læringsmål der arbejdes med i faget denne uge

#### Overordnede læringsmål fra studie ordningen

- **_Den studerende har viden om og forståelse for_**
  - Netværkstrusler
  - Forskellige sniffing strategier og teknikker
  - Netværk management (overvågning/logning, snmp)
- **_Den studerende kan_**

  - Overvåge netværk samt netværkskomponenter, (f.eks. IDS eller IPS, honeypot)
  - Identificere sårbarheder som et netværk kan have

- **_Den studerende kan håndtere udviklingsorienterede situationer herunder_**
  - Udfærdige en rapport om de sårbarheder et netværk eventuelt skulle have (red team report)
  - Monitorere og administrere et netværks komponenter

#### Læringsmål den studerende kan bruge til selvvurdering

- Den studerende kan forklare hvad aktiv monitorering er
- Den studerende kan forklare attack surface og interne vs. eksterne tools.
- Den studerende kan scanne med passende værktøjer

## Afleveringer

- ingen

## Skema

### Mandag

|  Tid  | Aktivitet                                          |
| :---: | :------------------------------------------------- |
| 8:15  | Introduktion til dagen                             |
| 8:30  | Gennemgang af semester evalueringer                |
| 9:00  | Evaluering og diskussion af opgaver fra sidste uge |
| 10:30 | Opgaver K1/K4                                      |
| 11:30 | Frokost                                            |
| 12:15 | Opgaver K1/K4                                      |
| 15:30 | Fyraften                                           |

## Kommentarer

- 8 lektioner med underviser

## Links

- [Mastering the Nmap Scripting Engine - Fyodor & David Fifield](https://youtu.be/M-Uq7YSfZ4I)
