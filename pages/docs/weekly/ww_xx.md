---
Week: xx
Content: XXX
Material: xxx
Initials: XXXX
# hide:
#  - footer
---

# Uge xx - *Titel*

## Emner

## Ugens emner er

- ..
- ..

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- ..
- ..

### Læringsmål der arbejdes med i faget denne uge

**Overordnede læringsmål fra studie ordningen:**

- **Viden:** ..
- **Færdigheder:** ..
- **Kompetencer:** ..

**Læringsmål den studerende kan bruge til selvvurdering**

- **Viden:** ..
- **Færdigheder:** ..
- **Kompetencer:** ..

## Afleveringer

- ...

## Skema

### Mandag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 8:15  | Introduktion til dagen |
| 8:30  | Opgaver fra sidste uge |
| 09:30 | Opgaver K1/K4          |
| 11:30 | Frokost                |
| 12:15 | Opgaver K2/K3          |
| 15:30 | Fyraften               |

## Kommentarer

- ..

## Links

- none supplied