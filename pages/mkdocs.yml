site_name: F23 Netværks- og kommunikationssikkerhed
site_url: https://ucl-pba-its.gitlab.io/23f-its-nwsec/
repo_url: https://gitlab.com/ucl-pba-its/23f-its-nwsec/
edit_uri: /ucl-pba-its/23f-its-nwsec/-/tree/main/pages/docs

#structure
nav:
  - 'Hjem': index.md
  - 'Ugeplaner':
      - 'Uge 06 - Introduktion til faget': 'weekly/ww_06.md'
      - 'Uge 07 - Forstå netværk - OSI modellen og network hardware': 'weekly/ww_07.md'
      - 'Uge 08 - Netværkstopologi: subnets, IP adresser, VLANs': 'weekly/ww_08.md'
      - 'Uge 09 - Bygge netværk: Virtuelle netværk, fysiske netværk': 'weekly/ww_09.md'
      - 'Uge 10 - Netværks protokoller: Wireshark, sniffe trafik': 'weekly/ww_10.md'
      - 'Uge 11 - Monitorering: SNMP, syslog, Prometheus, Grafana': 'weekly/ww_11.md'
      - 'Uge 12 - Network services og applikationer: DNS, Webapps, APIer': 'weekly/ww_12.md'
      - 'Uge 13 - Recap': 'weekly/ww_13.md'
      - 'Uge 15 - Sikkerhed på netværk - OSI modellen igen': 'weekly/ww_15.md'
      - 'Uge 16 - Hardware sikkerhed: wireless sniffing, 802.1x, firewall rules': 'weekly/ww_16.md'
      - 'Uge 17 - Intrusion Detection og Prevention (IDS/IPS)': 'weekly/ww_17.md'
      - 'Uge 18 - Hardware sikkerhed lag 5+: application layer security. NG firewalls. Certificates': 'weekly/ww_18.md'
      - 'Uge 19 - Topologier: segmentation, DMZ, non-routable networks, lateral movement': 'weekly/ww_19.md'
      - 'Uge 20 - Aktiv monitoring: sniffing, DNS/NS, scanning, shodan, nmap': 'weekly/ww_20.md'
      - 'Uge 21 - Eksamens recap, og emne gennemgang': 'weekly/ww_21.md'

  - 'Opgaver':
      - 'Opgave 1 - Fagets læringsmål': 'exercises/01_learning_goals.md'
      - 'Opgave 2 - OSI Model': 'exercises/02_osi_model.md'
      - 'Opgave 3 - Fejlfinding': 'exercises/03_fejlfinding.md'
      - 'Opgave 4 - Specs': 'exercises/04_specs.md'
      - 'Opgave 5 - Internet speeds': 'exercises/05_internet_speeds.md'
      - 'Opgave 6 - More internet speed': 'exercises/06_more_internet_speed.md'
      - 'Opgave 6b - Optional: Din egen iperf server': 'exercises/06b_optional_egen_iperf_server.md'
      - 'Opgave 7 - ipam': 'exercises/07_ipam.md'
      - 'Opgave 8 - Netværksdiagrammer': 'exercises/08_nw_diagram.md'
      - 'Opgave 9 - Protokoller': 'exercises/09_protokoller.md'
      - 'Opgave 10 - Trafikanalyse': 'exercises/10_trafikanalyse.md'
      - 'Opgave 11 - DNS basics': 'exercises/11_dns_basics.md'
      - 'Opgave 12 - DNS Rekognosering': 'exercises/12_recon.md'
      - 'Opgave 13 - DNS mini ctf': 'exercises/13_digdug.md'
      - 'Opgave 14 - recap: wirshark basics': 'exercises/14_wireshark_basics.md'
      - 'Opgave 15 - recap: wirshark osi': 'exercises/15_wireshark_osi.md'
      - 'Opgave 16 - recap: wireshark nat': 'exercises/16_wireshark_NAT.md'
      - 'Opgave 17 - recap: logs': 'exercises/17_logs.md'
      - 'Opgave 18 - ARP basics': 'exercises/18_arp.md'
      - 'Opgave 19 - Ettercap': 'exercises/19_ettercap.md'
      - 'Opgave 20 - Ettercap DNS': 'exercises/20_ettercap_dns.md'
      - 'Opgave 21 - Wireless': 'exercises/21_wireless.md'
      - 'Opgave 22 - Routing': 'exercises/22_routing.md'
      - 'Opgave 23 - Dynamic Routing': 'exercises/23_dynamic_routing.md'
      - 'Opgave 24 - VPN': 'exercises/24_vpn_tunnel.md'
      - 'Opgave 25 - udp amplification attack': 'exercises/25_udp_amplification_attack.md'
      - 'Opgave 26 - IDS/IPS viden': 'exercises/26_ids_knowledge.md'
      - 'Opgave 27 - Suricata opsætning': 'exercises/27_ids_suricata_config.md'
      - 'Opgave 28 - IDS/IPS Regler': 'exercises/28_ids_rules.md'
      - 'Opgave 29 - Suricata Live': 'exercises/29_ids_suricata_live.md'
      - 'Opgave 30 - Certifikater': 'exercises/30_certifikater.md'
      - 'Opgave 31 - Lets encrypt': 'exercises/31_lets_encrypt.md'
      - 'Opgave 32 - Easy rsa pki': 'exercises/32_easy_rsa.md'
      - 'Opgave 33 - Mitm proxy': 'exercises/33_mitmproxy.md'
      - 'Opgave 34 - Mitre att&ck framework': 'exercises/34_mitre_intro.md'
      - 'Opgave 35 - Network segmentation og Mitre att&ck framework': 'exercises/35_network_segmentation.md'
      - 'Opgave 36 - APR/DHCP og Mitre att&ck framework': 'exercises/36_mitre_ARP.md'
      - 'Opgave 37 - DNS og Mitre att&ck framework': 'exercises/37_mitre_DNS.md'
      - 'Opgave 38 - Angrebsflade (attack Surface)': 'exercises/38_attack_surface.md'
      - 'Opgave 39 - Huller i suppen': 'exercises/39_attack_surface_reasons.md'
      - 'Opgave 40 - Aktive skanningsværktøjer': 'exercises/40_active_scanning_tools.md'
      - 'Opgave 41 - Repetitions forberedelse': 'exercises/41_recap_prep.md'
      - 'Opgave 99 - Repetition af faget': 'exercises/99_repetition.md'
      # - 'Opgave xx - xxxxxxx': 'exercises/xx_xxxx.md'

  - 'Dokumenter':
      - 'Lektionsplan': 'other_docs/lektionsplan.md'
      - 'Eksamensopgave': 'other_docs/eksamens_opgave.md'

extra:
  generator: True
  social:
    - icon: fontawesome/brands/gitlab 
      link: https://ucl-pba-its.gitlab.io/
      name: UCL IT Sikkerhed


#theme
theme:
  name: material
  logo: images/UCL_horisontal_logo_UK_neg_rgb.png
  locale: en
  features:
    - navigation.tabs
    - navigation.tabs.sticky
    - navigation.sections
    - navigation.instant
    - navigation.tracking
    - navigation.top
    - content.code.annotate
    - toc.follow

  palette:
    - scheme: default
      primary: blue grey
      accent: orange
      toggle:
        icon: material/toggle-switch
        name: Switch to dark mode
    - scheme: slate
      primary: blue grey
      accent: orange
      toggle:
        icon: material/toggle-switch-off-outline
        name: Switch to light mode
  font:
    text: Roboto
    code: Roboto Mono
  favicon: favicon.png
  icon:
    logo: logo
    repo: fontawesome/brands/gitlab

markdown_extensions:
  - toc:
      permalink: True
  - pymdownx.arithmatex:
      generic: true
  - pymdownx.highlight:
      anchor_linenums: true
  - pymdownx.superfences:
      custom_fences:
        - name: mermaid
          class: mermaid
          format: !!python/name:pymdownx.superfences.fence_code_format
  - pymdownx.inlinehilite
  - pymdownx.snippets
  - pymdownx.superfences
  - attr_list
  - md_in_html

plugins:
  - git-revision-date-localized:
      type: iso_datetime
      fallback_to_build_date: true
  - search:
      lang:
        - en
        - da
  - page-to-pdf:
      disableOnServe: True
      displayHeaderFooter: True
  - htmlproofer: #https://github.com/manuzhang/mkdocs-htmlproofer-plugin
      enabled: !ENV [ENABLED_HTMLPROOFER, True] #disable the plugin locally using: set ENABLED_HTMLPROOFER=false
      raise_error_after_finish: True
      validate_external_urls: True
      raise_error_excludes:
        403: ['*'] # forbidden, usually means it is there, but CI is not allowed to read it.
        523: ['*'] # internal server thing on cloudflare. usually temporary
        -1: ['https://drive.google.com/drive/folders/1gPSNA-2OF9dDJTdhUOcn2N3zPjRBUSWm?usp=sharing']
        # 404: ['*']
        # 504: ['https://www.mkdocs.org/']

extra_javascript:
  - javascripts/mathjax.js
  - https://polyfill.io/v3/polyfill.min.js?features=es6
  - https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js
